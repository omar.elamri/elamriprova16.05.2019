import { Component } from '@angular/core';
import { DatiService } from './services/dati.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'verifica';
  dato:number = 0;
  datiMeteo:any[]=[];

  constructor(private datiService:DatiService){}

  onClick(){
    this.datiService.getDatiMeteo().subscribe(
      (dati:any[]) => this.datiMeteo=dati
    )
    this.dato = this.datiService.getDato();
  }
}